using System;

namespace BinarySearchTask
{
    public static class ArrayExtension
    {
        public static int? BinarySearch(int[] source, int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source), "Array is null.");
            }

            int? index = null;

            if (source.Length == 0)
            {
                return index;
            }

            int mediumValue = source.Length / 2;

            if (value < source[mediumValue])
            {
                for (int i = mediumValue; i >= 0; i--)
                {
                    if (source[i] == value)
                    {
                        index = i;
                        break;
                    }
                }
            }
            else if (value > source[mediumValue])
            {
                for (int i = mediumValue; i < source.Length; i++)
                {
                    if (source[i] == value)
                    {
                        index = i;
                        break;
                    }
                }
            }
            else if (value == source[mediumValue])
            {
                index = mediumValue;
            }

            return index;
        }
    }
}
